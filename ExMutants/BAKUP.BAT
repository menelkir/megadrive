@echo off
cd ..
xcopy xmutant\*.* xmbak\
zip xmutant xmutant\*.* -P -r -e
cd \zbak
del xmutant.bak
ren xmutant.zip xmutant.bak
cd ..
xcopy xmutant.zip \zbak
del b:xmutant.*
xcopy xmutant.zip b:
del xmutant.zip
cd \xmutant
echo All done ..
